import RPi.GPIO as GPIO
from time import sleep

class Seat:

    gpio_map = {
        'seat_down': 26,
        'seat_up': 13,
        'back_down': 6,
        'back_up': 19,
    }

    def __init__(self, name):
        self.name = name
        GPIO.setmode(GPIO.BCM)
        for action in self.gpio_map:
            GPIO.setup(self.gpio_map[action], GPIO.OUT)
        self.reset()

    def test(self):
        print "Launching test sequence ..."
        for action in self.gpio_map:
            self.move(action=action, duration=2)

    def move(self, action, duration):
        print "Move %(action)s for %(duration)i secs (pin %(pin)i)" % {
            'action': action,
            'duration': duration,
            'pin': self.gpio_map[action],
        }
        GPIO.output(self.gpio_map[action], False)
        sleep(duration)
        GPIO.output(self.gpio_map[action], True)

    def reset(self):
        for action in self.gpio_map:
            GPIO.output(self.gpio_map[action], True)