from flask import Flask, render_template
from seat import Seat

app = Flask(__name__)
mycroft = Seat('Mycroft')

@app.route("/")
@app.route("/<action>")
def move_seat(action=None):
    if action is not None:
        mycroft.move(action=action, duration=2)

    template_data = {
        'title' : action,
    }
    return render_template('main.html', **template_data)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
