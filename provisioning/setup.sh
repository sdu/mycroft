#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install python-pip python-dev nginx
sudo apt-get install python3-gpiozero python-gpiozero
sudo apt-get install supervisor
sudo pip install virtualenv

virtualenv mycroftenv
source mycroftenv/bin/activate
pip install gunicorn flask
pip install RPi.GPIO

sudo rm /etc/nginx/sites-enabled/default
sudo vim /etc/nginx/sites-available/mycroft


sudo ln -s /etc/nginx/sites-available/mycroft /etc/nginx/sites-enabled/
sudo nginx -t
sudo service nginx restart

sudo vim /etc/supervisor/conf.d/mycroft.conf

sudo mkdir /var/log/gunicorn

sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start mycroft


